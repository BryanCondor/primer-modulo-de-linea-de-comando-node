# Modulos de linea de comando en node: MacOs - Gnu/Linux

para ejecutar los archivo .bin del package.json

debemos de agregarlo en los path un sentencia

1. Comando para mostrar donde estan instalados los .bin (modulo de linea de comandos de node)

retorna la ruta de donde se encuentra instalados los bin, según donde te encuentras

```bash
npm bin
```

2. agregados a nuestro variable PATH este comando para que encuentre dinámicamente la ubicación.

```bash
export PATH=$(npm bin):$PATH
```

Y listo, ya podemos tener esa referencia a los bin a nivel global,

Ojo el Shabang (nombre que se le da a la combinación de #! que indica donde ejecutarse) para node debe ser el siguiente

Ejemplo: modulo de linea de comando de node

```bash
#!/usr/bin/env node

const [, , ...args] = process.argv;

console.log(`Hey Hola ${args}`);
```

---

Referencia

[https://stackoverflow.com/questions/9679932/how-to-use-executables-from-a-package-installed-locally-in-node-modules](https://stackoverflow.com/questions/9679932/how-to-use-executables-from-a-package-installed-locally-in-node-modules)